// pages/login/register.js
const app = getApp() //引用app.js中的函数的获取的方法
Page({

  /**
   * 页面的初始数据
   */
  //设置变量，赋值为空
  data: {
    True_name:'',
    Study_number:'',
    Classroom:'',
    Contact_info:''

  },
//调用change函数
  changeTrue_name:function(e) {
    //触发函数并赋值
    this.setData({
      name:e.detail.value
    })
  },


 changeStudy_number:function(e) {
      this.setData({
        number:e.detail.value
      })   
  },
  changeClassroom:function(e) {
    this.setData({
      classroom:e.detail.value
    })
  },
  changeContact_info:function(e) {
    this.setData({
      contact_info:e.detail.value
    })
  },
 //实现提交功能上传服务器
  bindSubmit:function(e) {
    wx.request({
      url: 'https://www.itools.top:10086/api/keystone/',
      data:  {
        openid: wx.getStorageInfoSync('openid'),
        globalData: JSON.stringify(app.globalData.useInfo),
        name: this.data.name,
        number: this.data.number,
        classroom: this.data.classroom,
        contact_info: this.data.contact_info,
      },
      success: res => {
        console.log('res1',res)
       if (res.data.is_register) {
         wx.redirectTo({
          url: '../home', //上传成功返回指定页面
         })
       } else {
        // this.openAlert(res.data.data)
       }
      },
      fail: res => {
      },
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})