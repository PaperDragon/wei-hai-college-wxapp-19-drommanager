//app.js
App({
  onLaunch: function () {
    // 展示本地存储能力
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)

    // 登录
    // wx.login({
    //   success: res => {
    //     console.log('res', res)
    //     // 发送 res.code 到后台换取 openId, sessionKey, unionId
    //     wx.request({
    //       url: 'https://zjgsujiaoxue.applinzi.com/index.php/Api/weixin', //后端服务器地址
    //       date: {
    //         'code': res.code,
    //         'from': 'wx085894dee0c3d2f5' //appid
    //       },
    //       success: function (res) {
    //         console.log(res.data)
    //         //将sessionid保存到本地storage
    //         wx.setStorageSync('openid', res.data.openid)
    //         if (!res.data.is_login) {
    //           wx.showModal({
    //             title: '提示',
    //             content: '请先注册',
    //             showCancel: false,
    //             confirmText: '确定',
    //             success: function (res) {
    //               wx.navigateTo({
    //                 url: '/pages/login/userlogin', //跳转到指定页面
    //               })
    //             }
    //           })
    //         }
    //       },
    //     })
    //   }
    // })
    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              // 可以将 res 发送给后台解码出 unionId
              this.globalData.userInfo = res.userInfo

              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              if (this.userInfoReadyCallback) {
                this.userInfoReadyCallback(res)
              }
            }
          })
        }
      }
    })
  },
  globalData: {
    userInfo: null
  }
})