
from django.shortcuts import render, HttpResponse
from django.http import HttpResponse,JsonResponse

# Create your views here.
from imageapp import models
from WXapp import settings
import os

#将图片保存到服务器中
def imgupload(request):
    # 先GET请求http://127.0.0.1:8000/image/upload.html  后POST请求form
    if request.method == 'POST':
        img = request.FILES.get('img')#POST请求 得到文件和及属性
        img_path = settings.STATICFILES_DIRS #路径C:\Users\sxs\Desktop\wei-hai-college-wxapp-19-drommanager\after\WXapp\static/
        print(img.name)
        img_url = ''.join(img_path) + '/image' + '/' + ''.join(img.name)
        #转化出一个存储路径C:\Users\sxs\Desktop\wei-hai-college-wxapp-19-drommanager\after\WXapp\static + '/image' + '/' + '{{img.name}}'
        print(img_url)
        # 将图片以二进制的形式写入
        with open(img_url, 'wb') as f:
            for data in img.chunks():
                f.write(data)
        # 将路径转化一下，转为href的形式，然后存入数据库，发到后端
        pic_data = '127.0.0.1:8000/static/image' + '/' + img.name
        models.imginfo.objects.create(img=pic_data)

        img_href = models.imginfo.objects.values().last()
        print(img_href)
        return render(request, 'imageapp/upload.html',
                      {'img_href': img_href})
    return render(request, 'imageapp/upload.html')


# 减小服务器压力 使用函数处理图片
def get_img(request, imgpath):
    print(imgpath)
    img_path = settings.STATICFILES_DIRS + "/" + imgpath
    print(img_path)
    image_data = open(img_path, "rb").read()
    print(img_path)
    return HttpResponse(image_data, content_type="image/png")

