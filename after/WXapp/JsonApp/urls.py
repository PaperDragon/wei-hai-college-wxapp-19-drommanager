from django.urls import path
from JsonApp import views


urlpatterns = [
    #第一次注册和加载
    path('judge/', views.judge),
    path('zuce/', views.zuce),

    #获取本人信息
    path('teacher_own/',views.teacher_own),
    path('student_own/',views.student_own),

    #统计
    path('tongxunlu/',views.tongxunlu),
    path('biaoge/', views.biaoge),
    path('biaoge3/', views.biaoge3),
    path('biaoge4/', views.biaoge4),
    #path('biaoge5/', views.biaoge5),

    #二维码解析实施扣分
    path('jiexi/',views.jiexi),
    path('wmid/',views.wmid),
    path('score/',views.score),

    #文件传输
    path('images/',views.images),

    #获取扣分记录
    path('record/',views.record),
    path('recordA0/',views.recordA0),

    #等级评定
    path('grade/', views.grade)
]
