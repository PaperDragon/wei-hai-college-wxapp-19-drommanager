from django.db import models

#学生登记表
class studentBasic(models.Model):
    stid = models.AutoField(primary_key=True)
    studentClassYearId = models.CharField(max_length=255, null=True)   #学生所在年级
    studentId = models.CharField(max_length=11, null=True)             #学生学号
    name = models.CharField(max_length=255, null=True)                 #学生姓名
    studentMajor = models.CharField(max_length=255, null=True)         #学生专业
    studentClassId = models.CharField(max_length=255, null=True)       #学生所在班级
    studentOfTeacher = models.CharField(max_length=255, null=True)     #学生的班主任
    openid = models.CharField(max_length=255, null=True)               #微信Openid
    code = models.CharField(max_length=255, null=True)                 #微信code
    studentUnion = models.BooleanField(default=False)                  #学生是否为学生会
    avatarUrl= models.CharField(max_length=255, null=True)             #微信头像
    region = models.CharField(max_length=16, null=True)                #姓名的第一个字的字母
    wmids = models.CharField(max_length=255, null=True)                #床铺信息

#班主任登记表
class teacherBasic(models.Model):
    teid = models.AutoField(primary_key=True)
    teacherClassYearId = models.CharField(max_length=16, null=True) #班主任所在年级
    teacherMajor = models.CharField(max_length=255, null=True)      #班主任所带专业
    teacherClassId = models.CharField(max_length=255, null=True)    #班主任所在班级
    teacherId = models.CharField(max_length=255, null=True)         #班主任工号
    name = models.CharField(max_length=255, null=True)              #班主任姓名
    openid = models.CharField(max_length=255, null=True)            #微信Openid
    code = models.CharField(max_length=255, null=True)              #微信code
    avatarUrl = models.CharField(max_length=255, null=True)         #微信头像
    admin=models.BooleanField(default=False)                        #是否为学工办

#学生扣分表
class score(models.Model):
    scid = models.AutoField(primary_key=True)
    studentId = models.CharField(max_length=11, null=True)    #学号
    code= models.CharField(max_length=255, null=True)         #与图片绑定的标示
    because = models.CharField(max_length=255, null=True)     #被扣学生原因
    situation= models.CharField(max_length=255, null=True)    #被扣学生情景
    fraction= models.CharField(max_length=255, null=True)     #被扣学生扣分
    myid = models.CharField(max_length=255, null=True)        #扣分人学号
    beizhu= models.CharField(max_length=255, null=True)       #备注
    name = models.CharField(max_length=255, null=True)        #学生姓名
    time = models.CharField(max_length=255, null=True)        #扣分时间

#扣分图片表
class image(models.Model):
    scid = models.AutoField(primary_key=True)
    code= models.CharField(max_length=255, null=True)         #与扣分信息绑定的标示
    image = models.CharField(max_length=255, null=True)       #image path

class renyuan(models.Model):
    scid = models.AutoField(primary_key=True)
    teacherid=models.CharField(max_length=255, null=True)
    name= models.CharField(max_length=255, null=True)
    first = models.CharField(max_length=255, null=True)
    last=models.CharField(max_length=255, null=True)