"""WXapp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path
from admin_app import views

urlpatterns = [
    # 登陆
    path('login/', views.login),
    path('logout/', views.logout),
    path('manage/', views.manage),

    #首页
    path('index/',views.index),

    # 学生部分路由
    path('student_list/', views.student_list),
    path('student_add/', views.student_add),
    path('student_del/', views.student_del),
    path('student_edit/', views.student_edit),
    path('student_adds/', views.student_adds),
    # 老师部分路由
    path('teacher_list/', views.teacher_list),
    path('teacher_add/', views.teacher_add),
    path('teacher_del/', views.teacher_del),
    path('teacher_edit/', views.teacher_edit),
    path('teacher_adds/', views.teacher_adds),
    #学生会部分
    path('student_union/',views.student_union),
    path('studunion_add/',views.studunion_add),
    path('studunion_adds/',views.studunion_adds),
    path('studunion_del/',views.studunion_del),

    #显示扣分日志
    path('worklog/', views.worklog),
    path('worklog_del/', views.worklog_del),

    #部分功能的实现
    path('Screening/',views.Screening),
]
