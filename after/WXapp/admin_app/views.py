from django.shortcuts import render, redirect, HttpResponse
from JsonApp import models
from admin_app import models as models2
from django.contrib import messages
import xlrd
from functools import wraps


# 用户登陆
def login(request):
    # 判断用户提交的请求
    # 如果是git请求
    if request.method == 'GET':
        # 返回登陆页面
        return render(request, 'admin_app/login.html')
    # 如果是post请求
    elif request.method == 'POST':
        # 获取用户提交的用户名密码
        # pub_name = request.POST.get('pub_name')
        username = request.POST.get('username')
        userpwd = request.POST.get('userpwd')
        # 校验数据的数据
        if models2.admin_list.objects.filter(username=username, userpwd=userpwd):
            # 如果有返回一个student_list页面
            # 添加cookie值做验证
            ret =  redirect('/admin/manage/')
            ret.set_signed_cookie('is_login', 'login', salt='s88')
            return ret
        # 没有返回login页面
        else:
            return render(request, 'admin_app/login.html', {'error': "账号或密码错误"})


# 判断登陆的装饰器
def login_required(func):
    @wraps(func)
    def inner(request, *args, **kwargs):
        is_login = request.get_signed_cookie('is_login', salt='s88', default='')
        if is_login != 'login':
            # 没有登录
            # 弹出提示登陆窗口
            messages.success(request, "请先登录")
            return redirect("/admin/login")
        ret = func(request, *args, **kwargs)
        return ret

    return inner


# 退出登陆
@login_required
def logout(request):
    # 定义返回页面
    # 清除cookie值
    ret = redirect('/admin/login/')
    ret.delete_cookie('is_login')
    return ret

#首页展示
def index(request):
    return render(request,'admin_app/index.html')

#展示总页面
def manage(request):

    return render(request, 'admin_app/manage.html' )

# 展示学生列表
@login_required
def student_list(request):
    # 获取所有出版社信息
    all_student = models.studentBasic.objects.all().order_by('stid')
    # 返回一个页面，页面包含出版社的信息
    return render(request, 'admin_app/students_list.html', {'all_student': all_student})


# 增加学生工功能
@login_required
def student_add(request):
    if request.method == 'POST':

        # 获取用户提交的的数据
        studentClass = request.POST.get('studentClassYearId')
        studentMajor = request.POST.get('studentMajor')
        name = request.POST.get('name')
        studentId = request.POST.get('studentId')
        studentClassId = request.POST.get('studentClassId')
        studentOfTeacher = request.POST.get('studentOfTeacher')

        i = len(studentId)

        # 数据库中有重复的名字
        if models.studentBasic.objects.filter(studentId=studentId):
            return render(request, 'admin_app/student_add.html', {'error': "该学生已存在"})

        if i != 11:
            return render(request, 'admin_app/student_add.html', {'error': "学号格式不正确"})

        if studentClass and studentMajor and name and studentId and studentClassId and studentOfTeacher:
            # 将数据新增到数据库中
            models.studentBasic.objects.create(studentClassYearId=studentClass, studentId=studentId, name=name,
                                               studentMajor=studentMajor, studentClassId=studentClassId,
                                               studentOfTeacher=studentOfTeacher)
            # 返回一个重定向到展示出版社的页面
            return redirect('/admin/student_list/')
        else:
            return render(request, 'admin_app/student_add.html', {'error': "请把信息补充完整"})
    return render(request, 'admin_app/student_add.html')


# 删除学生
@login_required
def student_del(request):
    # 获取要删除的数据的id
    pk = request.GET.get('pk')
    # 根据pk到数据库中删除
    models.studentBasic.objects.filter(pk=pk).delete()
    # 返回重定向到展示学生的页面
    return redirect('/admin/student_list/')


# 修改学生列表
@login_required
def student_edit(request):
    pk = request.GET.get('pk')
    pub_obj = models.studentBasic.objects.get(pk=pk)
    editid = pub_obj.studentId

    # 逻辑判断是什么请求
    if request.method == 'GET':
        # get请求
        # get返回一个页面 页面包含form表单
        return render(request, 'admin_app/student_edit.html', {'pub_obj': pub_obj})

    else:
        # post请求
        # 获取用户提交的出版社名称
        studentClass = request.POST.get('studentClassYearId')
        studentMajor = request.POST.get('studentMajor')
        name = request.POST.get('name')
        studentId = request.POST.get('studentId')
        studentClassId = request.POST.get('studentClassId')
        studentOfTeacher = request.POST.get('studentOfTeacher')
        # print(studentClass)
        # print(studentMajor)
        # print(name)
        # print(studentId)
        # print(studentClassId)
        # print(studentOfTeacher)
        i = len(studentId)
        # 逻辑判断
        # 判断学号是否为11位
        if i != 11:
            return render(request, 'admin_app/student_add.html', {'error': "学号格式不正确"})

        # 判断输入的学号是否已重复
        if studentId != editid:
            if models.studentBasic.objects.filter(studentId=studentId):
                return render(request, 'admin_app/student_add.html', {'error': "该学生学号已存在"})

        # 判断提交的信息是否都不为空
        if studentClass and studentMajor and name and studentId and studentClassId and studentOfTeacher:
            # 修改数据库内的内容
            pub_obj.studentId = studentId
            pub_obj.studentOfTeacher = studentOfTeacher
            pub_obj.studentClassYearId = studentClass
            pub_obj.studentMajor = studentMajor
            pub_obj.name = name
            pub_obj.studentClassId = studentClassId
            pub_obj.save()
            return redirect('/admin/student_list/')
        else:
            return render(request, 'admin_app/student_add.html', {'error': "请把信息补充完整"})

    # 重定向到网页
    return redirect('/admin/student_list/')


# 批量增加数据
@login_required
def student_adds(request):
    # 判断用户提交的请求方式
    # 如果是POST请求进行导入数据
    if request.method == 'POST':

        # 获取用户提交的excel表格
        file_obj = request.FILES.get('studentlist')

        # 解析用户提交的表格
        student = xlrd.open_workbook(filename=None, file_contents=file_obj.read(), on_demand=True)
        student_sheet = student.sheet_by_index(0)
        student_list0 = student_sheet.col_values(colx=0)
        student_list1 = student_sheet.col_values(colx=1)
        student_list2 = student_sheet.col_values(colx=2)
        student_list3 = student_sheet.col_values(colx=3)
        student_list4 = student_sheet.col_values(colx=4)
        student_list5 = student_sheet.col_values(colx=5)

        # 将数据导入数据库
        i = 1
        z = len(student_list2)
        studentClassYearId = student_list0
        studentId = student_list1
        name = student_list2
        studentMajor = student_list3
        studntClassId = student_list4
        studentOfTeacher = student_list5

        while i < z:
            models.studentBasic.objects.create(studentClassYearId=int(studentClassYearId[i]),
                                               studentId=int(studentId[i]), name=name[i], studentMajor=studentMajor[i],
                                               studentClassId=studntClassId[i], studentOfTeacher=studentOfTeacher[i])
            i += 1
        return redirect('/admin/student_list/')

    # 不是POST请求返回一个页面
    return render(request, 'admin_app/student_adds.html')


# 展示老师信息列表
def teacher_list(request):
    # 获取数据库中的数据
    all_teacher = models.teacherBasic.objects.all().order_by("teid")
    # 将数据返回到网页中
    return render(request, "admin_app/teacher_list.html", {"all_teacher": all_teacher})


# 增加教师信息
@login_required
def teacher_add(request):
    # 判断用户提交的请求
    # POST
    print("11111")
    if request.method == "POST":
        print("222222")
        # 获取用户提交的数据
        teacherClassYearId = request.POST.get("teacherClassYearId")
        teacherMajor = request.POST.get("teacherMajor")
        teacherClassId = request.POST.get("teacherClassId")
        teacherId = request.POST.get("teacherId")
        name = request.POST.get("name")
        # 逻辑判断

        if models.teacherBasic.objects.filter(teacherId=teacherId):
            return render(request, "admin_app/teacher_add.html", {"error": "该教师教工号已存在"})
        if teacherClassYearId and teacherId and name and teacherMajor and teacherClassId:
            # 添加到数据中
            print("333333")
            models.teacherBasic.objects.create(teacherClassYearId=teacherClassYearId, teacherMajor=teacherMajor,
                                               teacherClassId=teacherClassId, teacherId=teacherId, name=name)
            # 重定向到展示页面
            return redirect("/admin/teacher_list")
        else:
            print("444444")
            return render(request, "admin_app/teacher_add.html", {"error": "输入的信息不能为空"})
    # get请求返回一个页面
    return render(request, "admin_app/teacher_add.html")


# 删除教师信息
@login_required
def teacher_del(request):
    # 获取要删除的数据的id
    pk = request.GET.get('pk')
    # 根据pk到数据库中删除
    models.teacherBasic.objects.filter(pk=pk).delete()
    # 返回重定向到展示学生的页面
    return redirect('/admin/teacher_list/')


# 修该教师数据
@login_required
def teacher_edit(request):
    pk = request.GET.get('pk')
    pub_obj = models.teacherBasic.objects.get(pk=pk)
    editid = pub_obj.teacherId

    # 逻辑判断是什么请求
    if request.method == 'GET':
        # get请求
        # get返回一个页面 页面包含form表单
        return render(request, 'admin_app/teacher_edit.html', {'pub_obj': pub_obj})

    else:
        # post请求
        # 获取用户提交的出版社名称
        teacherClassYearId = request.POST.get("teacherClassYearId")
        teacherMajor = request.POST.get("teacherMajor")
        teacherClassId = request.POST.get("teacherClassId")
        teacherId = request.POST.get("teacherId")
        name = request.POST.get("name")

        # 逻辑判断
        # 判断输入的教工号是否已重复
        if teacherId != editid:
            if models.teacherBasic.objects.filter():
                return render(request, 'admin_app/student_add.html', {'error': "该学生学号已存在"})

        # 判断提交的信息是否都不为空
        if teacherClassYearId and teacherId and name and teacherMajor and teacherClassId:
            # 修改数据库内的内容
            pub_obj.teacherClassYearId = teacherClassYearId
            pub_obj.teacherMajor = teacherMajor
            pub_obj.teacherClassId = teacherClassId
            pub_obj.teacherId = teacherId
            pub_obj.name = name
            pub_obj.save()
            return redirect('/admin/teacher_list/')
        else:
            return render(request, 'admin_app/student_add.html', {'error': "请把信息补充完整"})


# 批量增加教师
@login_required
def teacher_adds(request):
    # 判断用户提交的请求方式
    # 如果是POST请求进行导入数据
    if request.method == 'POST':

        # 获取用户提交的excel表格
        file_obj = request.FILES.get('teachertlist')
        # print(file_obj)
        # 解析用户提交的表格
        teacher = xlrd.open_workbook(filename=None, file_contents=file_obj.read())
        teacher_sheet = teacher.sheet_by_index(0)
        teacher_list0 = teacher_sheet.col_values(colx=0)
        teacher_list1 = teacher_sheet.col_values(colx=1)
        teacher_list2 = teacher_sheet.col_values(colx=2)
        teacher_list3 = teacher_sheet.col_values(colx=3)
        teacher_list4 = teacher_sheet.col_values(colx=4)

        # 将数据导入数据库
        i = 1
        z = len(teacher_list2)
        teacherClassYearId = teacher_list0
        teacherMajor = teacher_list1
        teacherClassId = teacher_list2
        teacherId = teacher_list3
        teachername = teacher_list4

        while i < z:
            models.teacherBasic.objects.create(teacherClassYearId=int(teacherClassYearId[i]),
                                               name=teachername[i], teacherMajor=teacherMajor[i],
                                               teacherClassId=teacherClassId[i], teacherId=teacherId[i])
            i += 1
        return redirect('/admin/teacher_list/')

    # 不是POST请求返回一个页面
    return render(request, 'admin_app/teacher_adds.html')


# 展示学生会成员
def student_union(request):
    # 获取数据库中的数据
    pub_obj = models.studentBasic.objects.filter(studentUnion="1")
    # 将数据展示到页面
    return render(request, 'admin_app/student_union.html', {"pub_obj": pub_obj})


# 增添学会成员
def studunion_add(request):
    # 判断用户的请求方式
    # post请求
    if request.method == "POST":
        # 获取用户提交的成员
        student_id = request.POST.get('studentId')
        print(student_id)
        name = request.POST.get('name')
        student = models.studentBasic.objects.get(studentId=student_id)
        print(student)
        # 判断学号和id是否存在存在
        if models.studentBasic.objects.filter(studentId=student_id, name=name):
            num = 1
            # 存在的话给该成员的studentunion添加1
            student.studentUnion = num
            student.save()
            # 重定向到展示页面
            return redirect('/admin/student_union/')

    return render(request, "admin_app/student_unionadd.html")


# 删除学生会成员
def studunion_del(request):
    # 获取用户删除的数据,获取数据库中的内容
    pk = request.GET.get('pk')
    student = models.studentBasic.objects.get(stid=pk)
    print(student)
    # 删除数据库中的该成员的标识
    student.studentUnion = 0
    student.save()
    # 重定向到成员展示页面
    return redirect('/admin/student_union/')


# 批量添加成员
def studunion_adds(request):
    # 判断用户请求
    # 如果是post请求
    if request.method == 'POST':
        # 获取用户提交的excel表
        pub_obj = request.FILES.get('student_union_list')
        # 解析excel表
        student_excel = xlrd.open_workbook(filename=None, file_contents=pub_obj.read())
        student_sheet = student_excel.sheet_by_index(0)
        student_name = student_sheet.col_values(colx=0)
        student_id = student_sheet.col_values(colx=1)
        # 批量添加
        num = len(student_name)
        i = 1
        while i < num :
            student = models.studentBasic.objects.get(studentId=int(student_id[i]))
            student.studentUnion = '1'
            student.save()
            i += 1
        # 重定向到展示页面
        return redirect('/admin/student_union/')
    return render(request, 'admin_app/student_unionadds.html')

#显示工作日志
def worklog(request):
    #获取数据库里的内容
    all_worklog = models.score.objects.all().order_by('time')
    #返回一个页面
    return render(request,'admin_app/worklog.html',{"all_worklog":all_worklog})

#删除某条日志
def worklog_del(request):
    #获取用户要删除的数据id
    pk = request.GET.get('pk')
    #删除数据
    models.score.objects.filter(scid=pk).delete()
    #重定向到展示面板
    return redirect('/admin/worklog/')


# 筛选出相同班级学生
def Screening(request):
    # 获取数据库中的数据
    all_classyear = models.studentBasic.objects.values_list("studentClassYearId", flat=True).distinct()
    all_Major = models.studentBasic.objects.values_list("studentMajor", flat=True).distinct()
    all_classid = models.studentBasic.objects.values_list("studentClassId", flat=True).distinct()
    if request.method == "POST":
        # 获取用户提交的数据
        classid = request.POST.get('classid')
        major = request.POST.get('major')
        classyear = request.POST.get('classyear')
        # 筛选出符合条件的数据
        student = models.studentBasic.objects.filter(studentMajor__contains=major, studentClassYearId=classyear,
                                                     studentClassId=classid)
        studentclass = str(classyear)+'级'+str(major) + str(classid)+'班'
        # 返回到展示列表
        return render(request, 'admin_app/show.html', {'all_student': student , 'class':studentclass})
    # 将数据返回到网页
    return render(request, 'admin_app/screen.html',
                  {'all_classyear': all_classyear, 'all_major': all_Major, 'all_classid': all_classid})
