from django.apps import AppConfig


class NoRouteAppConfig(AppConfig):
    name = 'no_route_app'
