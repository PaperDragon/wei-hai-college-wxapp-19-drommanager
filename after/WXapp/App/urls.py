"""WXapp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path

from App import views

urlpatterns = [
    path('index/', views.index, name='index'),
    path('create_stu/', views.create_account, name = 'create_stu'),
    path('create_tea/', views.teacher_account, name = 'create_tea'),
    path('getid/', views.getid, name='getid'),
    path('get_student',views.getStudent, name='getStudent'),
    path('score/', views.score),
]
