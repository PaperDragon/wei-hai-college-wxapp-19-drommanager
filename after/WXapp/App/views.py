import requests
from django.shortcuts import render, HttpResponse
import json

# Create your views here.


# Create your views here.
# 这个函数是用来测试App是否存活，没有实际的用处，长得好看而已
from App import models


def index(request):
    return render(request, 'App/index.html')


def getid1(request):
    code = request.GET['code']
    res = requests.get(
        url='https://api.weixin.qq.com/sns/jscode2session?',
        params={
            'appid': 'wx197bcf7c304fd31c',
            'secret': '95935ecd92ec22d0512bae06fae36692',
            'js_code': code,
            'grant_type': 'authorization_code'
        }
    ).json()
    openid = res['openid']
    print(openid)
    return openid


# 检测openid
def getid(request):
    # 获取openid
    openid = getid1(request)
    # 关于是否注册的相关判断
    if models.student_openid.objects.filter(openid=openid):
        return HttpResponse('right_stu')
    else:
        if models.teacher_openid.objects.filter(openid=openid):
            return HttpResponse('right_tea')
        else:
            return HttpResponse('not found')


# 学生openid的添加
def stu_addid(request):
    openid = getid1(request)
    models.student_openid.objects.create(openid=openid)


# 教师openid的添加
def tea_addid(request):
    openid = getid1(request)
    models.teacher_openid.objects.create(openid=openid)


# 学生注册账号
def create_account(request):
    openid = getid1(request)
    stu_addid(request)
    # 获取注册信息
    name = request.GET.get('name')
    number = request.GET.get('ID')
    class_name = request.GET.get('zhuanye')
    class_id = request.GET.get('class')
    class_TrueName = str(class_name) + str(class_id)
    # 判断
    if models.student_list.objects.filter(number=number):
        if models.student_list_copy.objects.filter(number=number):
            return HttpResponse('Account Exist')
        else:
            models.student_list_copy.objects.create(number=number)
            return HttpResponse('OK')
    else:
        models.student_openid.objects.filter(openid=openid).delete()
        return HttpResponse('Student not exist')


# 教师注册账号
def teacher_account(request):
    openid = getid1(request)
    tea_addid(request)
    number = 000
    if models.teacher_list.objects.filter(number=number):
        if models.teacher_list_copy.objects.filter(number=number):
            return HttpResponse('Account Exist')
        else:
            models.teacher_list_copy.objects.create(number=number)
            return HttpResponse('OK')
    else:
        models.teacher_openid.objects.filter(openid=openid).delete()
        return HttpResponse('Teacher not exist')


# 用于教师端获取学生信息
def getStudent(request):
    student = models.student_list.objects.all()
    for infor in student:
        print(infor.name, infor.number)
        return HttpResponse(infor.name, infor.number)


# 学生扣分相关
def score(request):
    studentID = request.GET.get('studentID')
    studentuni = request.GET.get('collegeID')
    reason = request.GET.get('because')
    situation = request.GET.get('situation')

    reason1 = reason + ' ' + situation
    print(studentID, studentuni, reason, situation,)
    models.score.objects.create(student_number=studentID, studentuni_number=studentuni, reason=reason1)
    re = {'aaa': '666'}
    return HttpResponse(json.dumps(re))


