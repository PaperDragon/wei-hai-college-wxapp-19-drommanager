from django.db import models

# Create your models here.

#建立扣分规则表
class score_rule(models.Model):
    name = models.CharField(max_length=20)
    num = models.IntegerField
    id = models.BigAutoField(primary_key=True)


#建立扣分表
class score(models.Model):
    image_add = models.CharField(max_length=255)
    studentuni_number = models.CharField(max_length=20, null=True)
    student_number = models.CharField(max_length=20, null=True)
    time = models.DateTimeField(max_length=20, auto_now=True)
    reason = models.CharField(max_length=15, null=True)


#建立学生信息表
class student_list(models.Model):
    name = models.CharField(max_length=8)
    number = models.IntegerField(primary_key=True)
    class_name = models.ForeignKey('class_list', on_delete=models.CASCADE)


#建立班级信息表
class class_list(models.Model):
    name = models.CharField(max_length=20, primary_key=True)
    teacher = models.ForeignKey('teacher_list', on_delete=models.CASCADE, null=True)


#建立学生openid表
class student_openid(models.Model):
    openid = models.CharField(max_length=32)


#建立教师openid表
class teacher_openid(models.Model):
    openid = models.CharField(max_length=32)


#建立教师信息表
class teacher_list(models.Model):
    number = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=8)
    phone_num = models.IntegerField(null=True)


#建立学生副本表，以判断是否重复注册
class student_list_copy(models.Model):
    number=models.IntegerField()

#建立教师副本表，以判断是否重复注册
class teacher_list_copy(models.Model):
    number=models.IntegerField()

#建立学生会学生表
class studentunion(models.Model):
    name = models.CharField(max_length=10)
    number = models.IntegerField()

#学生会openid表
class stuunion_openid(models.Model):
    openid = models.CharField(max_length=32)








