Django==2.1.8
pytz==2020.1
sqlparse==0.3.1
uwsgi==2.0.15
requests==2.24.0