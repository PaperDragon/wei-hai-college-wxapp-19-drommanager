// studentUnion/pages/details/details.js
Page({
    data: {
      
  },

  onLoad: function (options) {
    const that = this
    const code = options.code
    wx.request({
      url: 'http://127.0.0.1:8000/jsonapi/details',
      data:{code:code},
      success(e){
        const name = e.data.name
        const studentId = e.data.studentId
        const clas = e.data.class
        const time = e.data.time
        const student = e.data.student
        const because = e.data.because
        const situation = e.data.situation
        const fraction = e.data.fraction
        const image = e.data.image
        that.setData({
          name,
          studentId,
          class:clas,
          time,
          student,
          because,
          situation,
          fraction,
          image
        })
      }
    })
  },
  previewImage: function(e){
    wx.previewImage({
        current: e.currentTarget.id, // 当前显示图片的http链接
        urls: this.data.image // 需要预览的图片http链接列表
    })
  },
})