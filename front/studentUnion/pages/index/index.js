Page({
  data: {
    title:"首页",
    avatarUrl:'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1599306884008&di=7312c04a55a0e68c0f710e4ef7c4946c&imgtype=0&src=http%3A%2F%2Favatar.yonglang.co%2Fsubscription%2Fwx%2Fa650f8a9-3091-457c-b5b7-419d936f8f81.jpg',
    name:'',
    collegeID:'',
    list: [
      {
        "selectedIconPath": "/studentUnion/imgs/home0.png",
        "iconPath": "/studentUnion/imgs/home.png",
        "text": "首页",
        "isActive":true,
      },
      {
        "selectedIconPath": "/studentUnion/imgs/Message0.png",
        "iconPath": "/studentUnion/imgs/Message.png",
        "text": "消息",
        "isActive":false,
      },
      {
        "selectedIconPath": "/studentUnion/imgs/my0.png",
        "iconPath": "/studentUnion/imgs/my.png",
        "text": "我的",
        "isActive":false,
      }
    ],
    garde: false
  },
  onItemChange(e){                        //底部tabBar切换函数
    const {index} = e.detail
    let {list} = this.data
    const title = list[index].text
    list.forEach((v,i) => i === index?v.isActive=true:v.isActive=false)
      this.setData({
        list,
        title
      })
  },
  ontoptop(){                                     //扫一扫函数
    const that = this
    wx.scanCode({
      onlyFromCamera: true,
      scanType:['qrCode'],
      success(res){
        const data = res.result
        wx.request({
          url: 'http://127.0.0.1:8000/jsonapi/jiexi',
          data: {'data' : data},
          success(res){
            console.log(res)
            if(res.data==='error'){
              wx.navigateTo({
                url: '../form/form?type=error',
              })
            }else{
              const building = res.data.building
              const room = res.data.room
              const bed = res.data.bed
              const collegeID = that.data.collegeID
              const wmid = res.data.wmid
              // console.log(building,room,bed,collegeID)
              wx.navigateTo({
                url: '../form/form?building='+building+'&room='+room+'&bed='+bed+'&collegeID='+collegeID+'&code='+that.data.code+'&wmid='+wmid+'&type=success',
              })
            }
          }
        })
      },
      fail(){
      },
      complete(){
      }
    })
  },
  onTop(r){
    const collegeID = this.data.collegeID
    wx.navigateTo({
      url: r.currentTarget.dataset.tapurl+'?ID='+collegeID,
    }) 
  },
  onLoad: function (options) {
    // console.log(options)
      var that=this
      const code = options.code
      that.setData({
        code
      })
      wx.request({
        url: 'http://127.0.0.1:8000/jsonapi/student_own/',
        data:{'code':that.data.code},
        success:function(res){
          // console.log(res)
          let list=res.data[0]
          const name = list.name
          const studentId = list.studentId
          const avatarUrl = list.avatarUrl
          const garde = list.garde
          that.setData({
            name,
            collegeID: studentId,
            avatarUrl,
            garde
          })
        }
      })
  },
})