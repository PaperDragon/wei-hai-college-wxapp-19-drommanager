Page({
  data: {
    list:[]

  },
  onLoad: function (options) {
    const that = this
    const studentID = options.ID
    wx.request({
      url: 'http://127.0.0.1:8000/jsonapi/record/',
      data:{'ID': studentID},
      success(e){
        console.log(e)
        const list = e.data
        that.setData({
          list,
          studentID
        })
      }
    })
  },
  supman(){
    wx.navigateTo({
      url: '/pages/Superman/index',
    })
  },
  details(e){
    const code = e.currentTarget.dataset.code
    wx.navigateTo({
      url: '../details/details?code='+code,
    })
  },
})