Page({
  data: {
    informetion:true,             //是否获得正确信息
    building:'请选择',
    room:'',
    bed:'',
    studentName:'',
    studentClass:'',
    studentID:'',
    collegeID:'',
    buildlist:['西一','西二','西三'],
    build:'西一',
    index0:0,
    becauseList:['被子未叠','床铺不整','其他'],
    because:'被子未叠',
    index1:0,
    situationList:['一般','严重'],
    situation:'一般',
    index2:0,
    fractionList:['1','2','3','4'],
    fraction:'1',
    index3:0,
    multiArray:[['西','东','南'],[1,2,3,4,5,6,7,8,9,]],
    multiIndex:[0,0],
    files: [],
    fractionTime:'',
    beizhu:''
  },
  formSubmit(){             //将formData提交至云端
    const that = this
    const formData = {
      'studentID':this.data.studentID,           //被扣分学生学号
      'collegeID':this.data.collegeID,           //扣分人学号
      'because':this.data.because,             //扣分原因
      'situation':this.data.situation,         //扣分情形
      'fraction':this.data.fraction,            //所扣分数
      'beizhu':this.data.beizhu,
      'code': this.data.code,
      'name': this.data.studentName
    }
    wx.request({
      url: 'http://127.0.0.1:8000/jsonapi/score/',
      data:formData,
      success(e){
        console.log(e)
        const num = e.data.num
        if(e.data.type==='success'){
          var nums = that.data.files.length
          for(let i=0;i<nums;i=i+1){
            wx.uploadFile({
              filePath: that.data.files[i],
              name: 'image',
              url: 'http://127.0.0.1:8000/jsonapi/images/',
              formData:{'ID':num,'code':that.data.code},
              success(e){
                console.log(e)
              }
            })
          }
          wx.showModal({
            title: '上传成功',
            showCancel:false,
            success(){
              wx.navigateBack({
                delta: 1,
              })
            }
          })
        }else{
          console.log('上传数据失败')
        }
      },
    })
  },
  finderror(){                      //发现信息错误
    this.setData({
      studentName:'',
      studentClass:'',
      studentID: '',
      informetion:false
    })
  },
  formReset(){        //清空按钮(未设置)
  },
  bindMultiPickerChange(e){
    const a = e.detail.value[0]
    const b = String(e.detail.value[1]+1)
    console.log(b)
    let direction
    if(a === 0){direction = '西'}else if(a === 1){direction = '东'}else{direction = '南'}
    this.setData({
      building:direction+b
    })
  },
  inputroomID(e){                              //自定义宿舍房间
    this.setData({
      room:e.detail.value
    })
  },
  inputbedID(e){                              //自定义床号
    this.setData({
      bed:e.detail.value
    })
  },
  onBecauseChange(e){                          //扣分原因
    const index1 = e.detail.value
    const because = this.data.becauseList[index1]
    this.setData({
      index1,
      because
    })
  },
  onSituationChange(e){                       //情形
    const index2 = e.detail.value
    const situation = this.data.situationList[index2]
    this.setData({
      index2,
      situation
    })
  },
  onFractionChange(e){                            //所扣分数
    const index3 = e.detail.value
    const fraction = this.data.fractionList[index3]
    this.setData({
      index3,
      fraction
    })
  },
  onbeizhu(e){
    this.setData({
      beizhu:e.detail.value
    })
  },
  chooseImage: function (e) {
    var that = this;
    wx.chooseImage({
        sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
        sourceType: ['camera'], // 可以指定来源是相册还是相机，默认二者都有
        success: function (res) {
            // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
            that.setData({
                files: that.data.files.concat(res.tempFilePaths)
            });
        }
    })
  },
  previewImage: function(e){
    wx.previewImage({
        current: e.currentTarget.id, // 当前显示图片的http链接
        urls: this.data.files // 需要预览的图片http链接列表
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (e) {
    // console.log(e)
    if(e.type==='success'){
      this.setData({
        building:e.building,
        room: e.room,
        bed: e.bed,
        collegeID : e.collegeID,
        code: e.code,
        wmid: e.wmid,
      })
      this.shuju()
    }else if(e.type==='error'){
      this.setData({
        informetion: false
      })
    }
    //从云端拉取扣分原因
  },
    //从云端拉取学生数据
  shuju(){
    const that=this
    wx.request({
      url: 'http://127.0.0.1:8000/jsonapi/wmid/',
      data:{"wmid": that.data.wmid},
      success(e){
        // console.log(e)
        const name = e.data[0].name
        const studentId = e.data[0].studentId
        const studentClassId = e.data[0].studentMajor + e.data[0].studentClassId
        that.setData({
          studentName: name,
          studentClass: studentClassId,
          studentID: studentId
        })
      }
    })
  }
})