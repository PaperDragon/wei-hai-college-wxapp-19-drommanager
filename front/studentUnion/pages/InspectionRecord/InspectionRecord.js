// studentUnion/pages/KFJL/KFJL.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list:[]

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const that = this
    const studentID = options.ID
    wx.request({
      url: 'http://127.0.0.1:8000/jsonapi/recordA0/',
      data:{'ID': studentID},
      success(e){
        console.log(e)
        const list = e.data
        that.setData({
          list
        })
      }
    })
  },
  details(e){
    const code = e.currentTarget.dataset.code
    wx.navigateTo({
      url: '../details/details?code='+code,
    })
  },
})
