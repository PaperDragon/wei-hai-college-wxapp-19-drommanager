
Page({

    /**
     * 页面的初始数据
     */
    data: {
        swiperImg: [
            { src: "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1600356161724&di=b271c1ffc480d9b9ba64b8dc66daa9f2&imgtype=0&src=http%3A%2F%2Fn.sinaimg.cn%2Fsd%2Ftransform%2Fw550h362%2F20171201%2F2vaZ-fypikwt0207137.jpg" },
            { src: "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1600356204254&di=a4c6239e7cc0ca1c1f094a6cf310e168&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fw%3D580%2Fsign%3D4e56a574ac6eddc426e7b4f309dab6a2%2F3274a12397dda144ed96e723b3b7d0a20df48623.jpg" },
            { src: "https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=1820678024,2532526074&fm=26&gp=0.jpg" }
        ],
        title: "首页",
        list: [{
                "selectedIconPath": "/teacher/icons/11.png",
                "iconPath": "/teacher/icons/1.png",
                "text": "首页",
                "isActive": true
            },
            {
                "selectedIconPath": "/teacher/icons/22.png",
                "iconPath": "/teacher/icons/2.png",
                "text": "通讯录",
                "isActive": false
            },
            {
                "selectedIconPath": "/teacher/icons/33.png",
                "iconPath": "/teacher/icons/3.png",
                "text": "统计",
                "isActive": false
            },
            {
                "selectedIconPath": "/teacher/icons/55.png",
                "iconPath": "/teacher/icons/5.png",
                "text": "我的",
                "isActive": false
            }
        ],
    },
    goToDetail1: function() {
        wx.navigateTo({
            url: '../biaoge/biaoge?id='+this.data.getTeacherOwn.teacherId
        })
    },
    goToDetail3: function() {
        wx.navigateTo({
            url: '../biaoge3/biaoge3?id='+this.data.getTeacherOwn.teacherId
        })
    },
    goToDetail4: function() {
        wx.navigateTo({
            url: '../biaoge4/biaoge4?id='+this.data.getTeacherOwn.teacherId
        })
    },
    goToDetail5: function() {
        wx.navigateTo({
            url: '../biaoge5/biaoge5?id='+this.data.getTeacherOwn.teacherId
        })
    },
    onItemChange(e) { //底部tabBar切换函数
        const { index } = e.detail
        let { list } = this.data
        const title = list[index].text
        list.forEach((v, i) => i === index ? v.isActive = true : v.isActive = false)
        this.setData({
            list,
            title
        })
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        console.log(options)
        var that=this
        const code = options.code
        that.setData({
          code
        })
        console.log(code)
        wx.request({
             url: 'http://127.0.0.1:8000/jsonapi/teacher_own/',
             data:{'code':that.data.code},
             success:function(res){
                  console.log(res)
                  let list=res.data[0]
                  console.log(list)
                  that.setData({
                    getTeacherOwn: list
             })
        }
   })
    },
    xinxiInfo() {
        wx.navigateTo({
            url: '../info/info'
        })
    },
    Toaddresslist() {
        wx.navigateTo({
            url: '../addresslist/addresslist'
        })
    }
})