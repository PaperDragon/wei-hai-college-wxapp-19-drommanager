//index.js
//获取应用实例
const app = getApp()

Page({
    data: {},
    
    onStudentUnion() {
        wx.getUserInfo({
            success(r) {
                const avatarUrl = r.userInfo.avatarUrl
                wx.redirectTo({
                    url: '../../studentUnion/pages/index/index?avatarUrl=' + avatarUrl,
                })
            }
        })
    },
    onTeacher(){ wx.getUserInfo({
        success(r) {
            const avatarUrl = r.userInfo.avatarUrl
            wx.redirectTo({
                url: '../../teacher/pages/index/index?avatarUrl=' + avatarUrl,
            })
        }
    })},
    onLoad: function() {},
    getUserInfo: function(e) {}
})