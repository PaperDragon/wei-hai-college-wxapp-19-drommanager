Page({
  data: {
    name:'',
    ID:'',
    grade:''
  },
//提交数据
  formSubmit(e){
    const that = this
    wx.setStorageSync('name', this.data.name)
    wx.setStorageSync('ID', this.data.ID)
    wx.setStorageSync('grade', this.data.grade)
    wx.getUserInfo({
      success(e){
        const avatarUrl = e.userInfo.avatarUrl
        that.setData({
          avatarUrl
        })
      // },
      // complete(){
        wx.login({
          success(r) {
            const form = {
              'code': r.code,
              'ID': that.data.ID,
              'name':that.data.name,
              'grade':that.data.grade,
              'openid':that.data.openid,
              'avatarUrl':that.data.avatarUrl
            }
            wx.request({
              url: 'http://127.0.0.1:8000/jsonapi/zuce/',
              data: form,
              success(res){
                console.log(res)
                const grade = res.data
                if(grade === 'error'){
                  console.log('2222')
                  wx.showToast({
                    title: '信息错误！',
                    icon:'none',
                    duration:2000
                  })
                }else{
                  console.log('1111')
                  wx.showToast({
                    title: '注册成功',
                    image:'../../allImgs/success.png'
                  })
                  wx.redirectTo({
                  url: '../../pages/index/index'
                  })
                }
              },
            })
          }
        })
      }
    })
  },
  //选择权限
  gradeInfo(e){
    this.setData({
      grade:e.detail.value
    })
  },
  //获取学号
  inputID(e){
    const ID = e.detail.value
    this.setData({
      ID
    })
  },
  //获取姓名
  inputName(e){
    const name = e.detail.value
    this.setData({
      name
    })
  },
  onLoad(res){
    wx.showToast({
      title: '请先注册',
      icon:'none'
    })
    var name = wx.getStorageSync('name')
    var ID = wx.getStorageSync('ID')
    var grade = wx.getStorageSync('grade')
    this.setData({
      name,
      ID,
      grade
    })
  },
})