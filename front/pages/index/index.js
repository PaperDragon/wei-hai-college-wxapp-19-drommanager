Page({
  data: {
  },
  onLoad: function (options) {
    // const that = this
    wx.showToast({
      title: '加载中',
      icon:'loading'
    })
    wx.login({                      //获取openid
        success(r) {
          const code = r.code
          wx.request({
            url: 'http://127.0.0.1:8000/jsonapi/judge/',
            data: {'code' : code},
            success(res){
              const grade = res.data.aaa
              if (grade === 'right_stu'){                    //若注册则获取头像并跳转
                    wx.redirectTo({
                      url: '/studentUnion/pages/index/index?code='+code,
                    })
              }else if(grade === 'right_tea'){
                    wx.redirectTo({
                      url: '/teacher/pages/index/index?code='+code,        //跳转老师页面
                    })
              }else{
                wx.navigateTo({
                  url: '../register/register',
                })
              }
            },
          })
        }
    })
  },
})