
Component({
     properties:{
       list:Array,
       messtion:{
         type:Boolean,
         value:false
       }
     },
     data: {
     },
     attached() {
     },
     methods: {
       switchTab(e) {
         const {index} = e.currentTarget.dataset
         this.triggerEvent("itemChange",{index})
       },
     }
   })