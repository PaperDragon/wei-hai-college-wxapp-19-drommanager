from django.db import models

# Create your models here.

class cal(models.Model):
    value_a = models.CharField(max_length=10)
    value_b = models.CharField(max_length=10)
    result = models.CharField(max_length=10)



class user(models.Model):
    id = models.AutoField(primary_key=True)
    true_name = models.CharField(max_length=100)
    sex = models.BooleanField()
    contact_info = models.CharField(max_length=20)
    wx_user_name = models.CharField(max_length=100)
    study_number = models.IntegerField()
    QR_info = models.CharField(max_length=100)
    classroom = models.CharField(max_length=20)


class buckle_rule(models.Model):
    buckle_item = models.TextField()
    buckle_mark = models.DecimalField(max_digits=4, decimal_places=1,)
    buckle_id = models.AutoField(primary_key=True)


class buckle_list(models.Model):
    id = models.AutoField(primary_key=True)
    buckle_id = models.IntegerField()
    img_url = models.URLField()
    buckle_sb = models.CharField(max_length=50)


class imginfo(models.Model):
    img = models.FileField(upload_to='media')
