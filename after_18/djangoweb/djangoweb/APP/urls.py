"""djangoweb URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path

from APP import views

urlpatterns = [
    path('getcontent/<str:classroom>/', views.getcontent, name='getcontent'),
    path('imgupload/', views.imgupload, name='imgupload'),
    path('imgshow/<str:imgpath>', views.get_img, name='imgshow'),
    path('getrule/', views.get_rule, name='get_rule'),
    path('insertrule/<str:item>/<str:mask>/', views.insert_rule, name='insert_rule'),
    path('keystone/', views.keystone, name='keystone'),
]
