from django.http import JsonResponse, HttpResponse
from django.shortcuts import render

# Create your views here.
from APP import models

from djangoweb import settings


# 主页空着不好看，加一点东西吧Paper Dragon
def index(request):
    return render(request, 'index.html')


# 返回联系方式信息，这里简单一写，后期可以升级更新
def getcontent(request, classroom):
    try:
        datas = {}
        listdata = []
        usercontent = models.user.objects\
            .values_list('true_name', 'contact_info')\
            .filter(classroom=classroom)
        datas['usercontent'] = list(usercontent)
        for d in datas['usercontent']:
            data_one = str(d[0]) + '_' + str(d[1])
            listdata.append(data_one)
            print(listdata)
        # 第一次修改 能看到的都是人才
        a = {}
        c = []
        for e in listdata:
            print("e", e)
            a['name'] = e
            c.append(a)
            print("c", c)

        return JsonResponse({'success': True, 'data': c})
    except Exception as e:
        return JsonResponse({'success': False, 'data': str(e)})


# 上传扣分的照片啊，哈哈哈哈，等着吧小老弟们
def imgupload(request):
    if request.method == 'POST':
        img = request.FILES.get('img')
        img_path = settings.MEDIA_ROOT
        img_url = img_path + '/' + img.name
        print(img_url)
        # 将图片以二进制的形式写入
        with open(img_url, 'wb') as f:
            for data in img.chunks():
                f.write(data)
        # 将路径转化一下，转为href的形式，然后存入数据库，发到后端
        pic_data = 'https://www.itools.top:10086/api/imgshow' \
                   + '/' + img.name
        models.imginfo.objects.create(img=pic_data)
        img_href = models.imginfo.objects.values().last()
        print(img_href)
        return render(request, 'upload.html',
                      {'img_href': img_href})
    return render(request, 'upload.html')


# 减小服务器压力 使用函数处理图片
def get_img(request, imgpath):
    print(imgpath)

    img_path = settings.MEDIA_ROOT + "/" + imgpath
    print(img_path)
    image_data = open(img_path, "rb").read()
    print(img_path)
    return HttpResponse(image_data, content_type="image/png")


# 获取扣分规则
def get_rule(request):
    if request.method == 'GET':
        try:
            data = {}
            rule_list = models.buckle_rule.objects.values()
            data['rule_list'] = list(rule_list)
            print(data['rule_list'])
            return JsonResponse({'success': True, 'data': data['rule_list']})
        except Exception as e:
            return JsonResponse({'success': False, 'data': str(e)})


# 对扣分规则使用GET方式进行 插入记录
def insert_rule(request, item, mask):
    print(item, mask)
    if request.method == "GET":
        try:
            models.buckle_rule.objects.\
                create(buckle_item=item, buckle_mark=mask)
            return JsonResponse({'success': True})
        except Exception as e:
            return JsonResponse({'success': False, 'data': str(e)})


# 验证用户是否能进行通过，返回登录状态
def keystone(request):
    if request.method == 'GET':
        try:
            name = request.GET['name']
            number = request.GET['classroom']
            contact_info = request.GET['contact_info']
            openid = request.GET['openid']
            global_Data = request.GET['globalData']
            print(name, number, contact_info, openid, global_Data)

            return JsonResponse({'success': 'is_register'})

        except Exception as e:
            return JsonResponse({'success': False, 'data': str(e)})

    #     try:
    #         name = request.POST['name']
    #         number = request.POST['classroom']
    #         contact_info = request.POST['contact_info']
    #         openid = request.POST['openid']
    #         global_Data = request.POST['globalData']
    #         print(name, number, contact_info, openid, global_Data)
    #         return render(request, 'keystone.html')
    #     except Exception as e:
    #         return JsonResponse({'success': False, 'data': str(e)})
    # elif request.method == 'GET':
    #     try:
    #         return render(request, 'keystone.html')
    #     except Exception as e:
    #         return JsonResponse({'success': False, 'data': str(e)})
